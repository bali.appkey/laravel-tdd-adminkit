<div class="col-12 col-md-6">
    <div class="row">
        <div class="col-12 col-md-6 pb-3 pb-md-0">
            {!! Form::label('image', 'Image', ['class' => 'mb-1']) !!}
            {!! Form::file('image', ['class' => 'form-control', 'id' => 'images']) !!}
        </div>
    </div>

    <div class="row  mt-3">
        <div class="col-12 pb-3 pb-md-0">
            {!! Form::label('name', 'Name', ['class' => 'mb-1']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            FilePond.registerPlugin(
                FilePondPluginFileEncode,
                FilePondPluginFileValidateSize,
                FilePondPluginFileValidateType,
                FilePondPluginImageExifOrientation,
                FilePondPluginImagePreview
            )
            let options
            let imageUrl
            const url = window.location

            options = {
                acceptedFileTypes: ['image/png', 'image/jng', 'image/jpeg'],
                maxFileSize: '2MB',
                allowMultiple : true,
                maxFiles: 2,
                labelIdle: 'Drag & drop gambar atau <span class="filepond--label-action">cari di file manager</span>',
            }

            if (url.pathname.includes('edit')) {
                imageUrl = document.getElementById('image').getAttribute('image')
                options.files = [{
                    source: imageUrl,
                    options: {
                        type: 'remote'
                    }
                }]
            }

            const filePond = FilePond.create(
                document.getElementById('image'), options
            )

            filePond.on('error', (error) => {
                console.log(error)
                if (error.main == 'File is too large') {
                    console.log(error.main)
                }
            })

            filePond.on('warning', (warning) => {
                console.log(warning)
                if (warning.body == 'Max files') {
                    console.log('Max file exceeded')
                }
            })

            filePond.on('addfile', (error, file) => {
                if (error) {
                    console.log('Oh no', error)
                    return
                }

                console.log('File added', file)
            })

            filePond.on('removefile', (error, file) => {
                if (error) {
                    console.log('Oh no', error)
                    return
                }

                console.log('File removed', file)
            })
        })


    </script>
@endpush
